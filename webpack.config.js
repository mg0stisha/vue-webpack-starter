const webpack = require('webpack');

module.exports = {
	entry: './src/client/components/app.coffee',
	output: {
		path: './build/dist',
		filename: 'app.client.js'
	},
	module: {
		loaders: [
			{ test: /\.coffee$/, exclude: 'node_modules', loader: "coffee-loader" },
			{ test: /\.js$/, exclude: 'node_modules', loader: "babel-loader" },
			{ test: /\.vue$/, exclude: 'node_modules', loader: "vue-loader" }
		]
	},
  plugins: [
    new webpack.optimize.UglifyJsPlugin({
	    compress: {
	      warnings: false,
	    },
	    output: {
	      comments: false,
	    },
    }),
  ]
}