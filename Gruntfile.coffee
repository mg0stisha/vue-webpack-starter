webpack = require 'webpack'

module.exports = (grunt) ->
  grunt.initConfig
    pkg: grunt.file.readJSON('package.json')

    clean:
      build: ['build']

    copy:
      build:
        files: [
          {
            cwd: 'assets/'
            expand: true
            src: '**/*'
            dest: 'build/assets/'
          }
          {
            src: 'package.json'
            dest: 'build/'
          }
          {
            src: 'typings.json'
            dest: 'build/'
          }
        ]

    pug:
      compile:
        options:
          data:
            debug: true
        files: [
          'build/dist/index.html': 'src/client/templates/index.pug'
        ]

    coffee:
      glob_to_multiple:
        expand: true
        cwd: 'src/server'
        src: ['**/*.coffee', '!**/*.spec.coffee']
        flatten: false
        dest: 'build/dist'
        ext: '.js'
      compile:
        files:
          'build/app.js': 'src/server/app.coffee'

    sass:
      build:
        options:
          style: 'compressed'
        files:
          'assets/css/app.css': 'assets/css/app.scss'

    symlink:
      explicit:
        src: 'node_modules'
        dest: 'build/node_modules'

    webpack:
      build:
        entry: './src/client/main.coffee'
        output:
          path: './build/dist',
          filename: 'app.client.js'
        module:
          loaders: [
            { test: /\.coffee$/, exclude: 'node_modules', loader: "coffee-loader" },
            { test: /\.js$/, exclude: 'node_modules', loader: "babel-loader" },
            { test: /\.vue$/, exclude: 'node_modules', loader: "vue-loader" },
            { test: /\.sass$/, exclude: 'node_modules', loader: "sass-loader" },
            { test: /\.pug$/, exclude: 'node_modules', loader: "pug-loader" }
          ]
        plugins: [
          new webpack.optimize.UglifyJsPlugin({
            compress:
              warnings: false
            output:
              comments: false
          }),
        ]

    watch:
      scripts:
        files: ['src/server/**/*.coffee', '!src/**/*.spec.coffee', 'src/views/*.pug', 'assets/**',]
        tasks: ['watch-build']
        options:
          spawn: false

  grunt.loadNpmTasks('grunt-contrib-clean')
  grunt.loadNpmTasks('grunt-contrib-copy')
  grunt.loadNpmTasks('grunt-contrib-pug')
  grunt.loadNpmTasks('grunt-contrib-coffee')
  grunt.loadNpmTasks('grunt-contrib-sass')
  grunt.loadNpmTasks('grunt-contrib-symlink')
  grunt.loadNpmTasks('grunt-webpack')
  grunt.loadNpmTasks('grunt-contrib-watch')

  grunt.registerTask 'gitignore', 'Generate build/.gitignore', ->
    grunt.file.write 'build/.gitignore', 'node_modules'

  grunt.registerTask('build', ['clean', 'webpack', 'pug', 'coffee', 'sass', 'copy', 'gitignore', 'symlink'])
  grunt.registerTask('watch-build', ['webpack', 'pug', 'coffee', 'sass', 'copy'])