App = require "./components/App.vue"
TestVue = require "./components/TestVue.vue"
TodoList = require "./components/TodoList.vue"
Vue = require "vue"
Router = require "vue-router"

# Tell Vue to use the vue-router, create a new Router instance
Vue.use(Router)
router = new Router()

# Route map
router.map {
  '/': {
    component: {
      template: '<p>Default router-view content</p>'
    }
  }
  '/todo-list': {
    component: TodoList
  }
  '/test-vue': {
    component: TestVue
  }
}

# Called before any of the transition hooks
router.beforeEach (transition) ->
  console.log "Attempting transition from #{transition.from.path} to #{transition.to.path}..."
  transition.next()

# Called after the activate transition hook has started
router.afterEach (transition) ->
  console.log "Successfully transitioned from #{transition.from.path} to #{transition.to.path}!"

# Start the router
router.start App, '#app'
