addItem = () ->
  name = this.newItem.trim()
  if name
    this.items.push { name: name }
    this.newItem = ''

removeItem = (index) ->
  this.items.splice index, 1

model = {
  newItem: ''
  items: [
    { name: 'Clean car' },
    { name: 'Learn more webpack' },
    { name: 'Read a book' }
  ]
}

module.exports = {
  data: () -> 
    return model
  methods:
    addItem: addItem
    removeItem: removeItem
}