# TEST: Adding new properties via $set allows them to be tracked for change detection
addNewProp = () ->
  this.$set('newProp', 'This was set using this.$set!')

# TEST: Data transition hook: called after activate, sets component data
routeData = (transition) ->
  console.log "Route Data Loading..."
  transition.next { msg: "Welcome to the Vue app (from routeData)!" }

# TEST: Watcher to see when data loading has completed
loadingRouteDataWatcher = (val, oldVal) ->
  console.log "$loadingRouteData old: #{oldVal}, new: #{val}"

# TEST: Event handler to see when data loading has completed
routeDataLoaded = () ->
  console.log "Route Data Loaded!"

# TEST: Activate transition hook: called when activating a new component
routeActivate = (transition) ->
  console.log "Route Activated!"
  transition.next()

# TEST: Deactivate transition hook: called when deactivating a component
routeDeactivate = (transition) ->
  console.log "Route Deactivated!"
  transition.next()

# TEST: canActivate transition hook: called to see whether the component can be activated
routeCanActivate = (transition) ->
  console.log "Route Can Activate!"
  true

# TEST: canDeactivate transition hook: called to see whether the component can be deactivated
routeCanDeactivate = (transition) ->
  console.log "Route Can Deactivate!"
  true

# Export the vue object
module.exports = {
  data: () ->
    return { msg: "Welcome to the Vue app!" }
  props: ['msgFromParent']
  watch:
    '$loadingRouteData': loadingRouteDataWatcher
  events:
    'route-data-loaded': routeDataLoaded
  route:
    data: routeData
    activate: routeActivate
    deactivate: routeDeactivate
    canActivate: routeCanActivate
    canDeactivate: routeCanDeactivate
  methods:
    addNewProp: addNewProp
}