express = require 'express'
path = require 'path'

# Create app instance
app = express()

# Define static paths
app.use '/assets', express.static path.join(__dirname, 'assets')
app.use '/node_modules', express.static path.join(__dirname, 'node_modules')
app.use '/dist', express.static path.join(__dirname, 'dist')

# Send index back for any route hit
app.get '/', (req, res, next) ->
  res.sendFile path.join(__dirname, 'dist/index.html')

# Listen for commands on the env-specified port (default 3000)
app.listen process.env.PORT or 8000, () ->
  console.log "Vue.js Test Grunt Webpack BLASTOFF!"